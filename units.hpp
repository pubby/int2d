#ifndef INT2D_UNITS_HPP
#define INT2D_UNITS_HPP

// Definitions of simple 2d units without much extra.
// Try to keep functions that work on these units in other files,
// such as in geometry.hpp

// REMEMBER: x=right, y=down.
//    x
//  +---->
//  |
// y|
//  v

#include <type_traits>
#include <utility>

namespace int2d 
{

    // Use this to switch components at compile-time using templates.
    template<std::size_t N>
    struct component_index : std::integral_constant<std::size_t, N>
    {
        static constexpr std::size_t index() { return N; }
    };

    // Call a function for each component
    template<typename Func>
    void components(Func func)
    {
        func(component_index<0>{});
        func(component_index<1>{});
    }

    // Map over a unit's components
    template<typename T, typename Func>
    T mapc(T t, Func func)
    {
        T ret;
        components([&](auto c) { ret[c] = func(t[c]); });
        return ret;
    }

    struct dimen
    {
        static constexpr std::size_t components = 2;

        int w;
        int h;

        int const& operator[](component_index<0>) const { return w; }
        int& operator[](component_index<0>) { return w; }
        int const& operator[](component_index<1>) const { return h; }
        int& operator[](component_index<1>) { return h; }
    };

    constexpr bool operator==(dimen lhs, dimen rhs)
    {
        return lhs.w == rhs.w && lhs.h == rhs.h;
    }

    constexpr bool operator!=(dimen lhs, dimen rhs)
    {
        return !(lhs == rhs);
    }

    constexpr bool operator<(dimen lhs, dimen rhs)
    {
        return lhs.w != rhs.w ? lhs.w < rhs.w : lhs.h < rhs.h;
    }

    constexpr dimen operator*(dimen lhs, int scale)
    {
        return { lhs.w * scale, lhs.h * scale };
    }

    constexpr dimen operator/(dimen lhs, int scale)
    {
        return { lhs.w / scale, lhs.h / scale };
    }

    constexpr dimen vec_mul(dimen dim, int v)
    {
        return dim * v;
    }

    constexpr dimen dimen_add(dimen d1, dimen d2)
    {
        return { d1.w + d2.h, d1.h + d2.h };
    }

    struct coord
    {
        static constexpr std::size_t components = 2;

        int x;
        int y;

        int const& operator[](component_index<0>) const { return x; }
        int& operator[](component_index<0>) { return x; }
        int const& operator[](component_index<1>) const { return y; }
        int& operator[](component_index<1>) { return y; }
    };

    constexpr bool operator==(coord lhs, coord rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    constexpr bool operator!=(coord lhs, coord rhs)
    {
        return !(lhs == rhs);
    }

    constexpr bool operator<(coord lhs, coord rhs)
    {
        return lhs.x != rhs.x ? lhs.x < rhs.x : lhs.y < rhs.y;
    }

    constexpr coord operator+(coord lhs, coord rhs)
    {
        return { lhs.x + rhs.x, lhs.y + rhs.y };
    }

    constexpr coord operator-(coord lhs, coord rhs)
    {
        return { lhs.x - rhs.x, lhs.y - rhs.y };
    }

    inline coord& operator+=(coord& lhs, coord rhs)
    {
        lhs = lhs + rhs;
        return lhs;
    }

    inline coord& operator-=(coord& lhs, coord rhs)
    {
        lhs = lhs - rhs;
        return lhs;
    }

    constexpr coord operator+(coord lhs)
    {
        return lhs;
    }

    constexpr coord operator-(coord lhs)
    {
        return { -lhs.x, -lhs.y };
    }

    constexpr coord vec_mul(coord crd, int v)
    {
        return { crd.x * v, crd.y * v };
    }

    struct rect
    {
        coord c;
        dimen d;

        // 'end'
        coord e() const { return { ex(), ey() }; }
        template<typename C>
        int e(C c) const { return e()[c]; }
        constexpr int ex() const { return c.x + d.w; }
        constexpr int ey() const { return c.y + d.h; }

        // 'rbegin'
        coord r() const { return { rx(), ry() }; }
        template<typename C>
        int r(C c) const { return r()[c]; }
        constexpr int rx() const { return c.x + d.w - 1; }
        constexpr int ry() const { return c.y + d.h - 1; }

        coord xy() const { return c; }

        coord exy() const { return { ex(), c.y }; }
        coord xey() const { return { c.x, ey() }; }
        coord exey() const { return { ex(), ey() }; }

        coord rxy() const { return { rx(), c.y }; }
        coord xry() const { return { c.x, ry() }; }
        coord rxry() const { return { rx(), ry() }; }

        coord nw() const { return c; }
        coord ne() const { return rxy(); }
        coord sw() const { return xry(); }
        coord se() const { return rxry(); }
    };

    constexpr bool operator==(rect lhs, rect rhs)
    {
        return lhs.c == rhs.c && lhs.d == rhs.d;
    }

    constexpr bool operator!=(rect lhs, rect rhs)
    {
        return !(lhs == rhs);
    }

    constexpr bool operator<(rect lhs, rect rhs)
    {
        return lhs.c != rhs.c ? lhs.c < rhs.c : lhs.d < rhs.d;
    }

    template<typename X, typename Y>
    coord make_coord(X x, Y y)
    {
        return coord{ static_cast<int>(x), static_cast<int>(y) };
    }

    template<typename W, typename H>
    coord make_dimen(W w, H h)
    {
        return dimen{ static_cast<int>(w), static_cast<int>(h) };
    }

} // namespace int2d

#endif
