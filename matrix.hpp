#ifndef INT2D_MATRIX_HPP
#define INT2D_MATRIX_HPP

// Some very crude matrix types.
// Functionality is added as-needed; YAGNI!

#include <array>

#include "geometry.hpp"
#include "units.hpp"

namespace int2d {

    namespace impl
    {
        // Simplifies a number of quarter turns into an equivalent direction
        // in the range [0, 4)
        // TODO: only works with 2's compliment at the moment.
        constexpr int _simplify_quarter_turns(int turns) { return turns & 3; }
    }

    // An integer-based 2d transformation matrix.
    // REMEMBER: x=right, y=down. This affects rotations!
    template<typename T>
    struct integer_matrix_3x3
    {
        static_assert(std::is_integral<T>::value, "must be integral");

        auto const& operator[](std::size_t i) const { return arr[i]; }
        auto& operator[](std::size_t i) { return arr[i]; }

        std::array<std::array<T, 3>, 3> arr; // [row][col]; [y][x]

        static integer_matrix_3x3 identity()
        {
            return integer_matrix_3x3
            {{{
                {{ 1, 0, 0 }},
                {{ 0, 1, 0 }},
                {{ 0, 0, 1 }},
            }}};
        }

        static integer_matrix_3x3 rotate_cw(int n = 1)
        {
            int sin;
            int cos;
            switch(impl::_simplify_quarter_turns(n))
            {
            default:
            case 0: // 0 deg
                sin = 0;
                cos = 1;
                break;
            case 1: // 90 deg
                sin = 1;
                cos = 0;
                break;
            case 2: // 180 deg
                sin = 0;
                cos = -1;
                break;
            case 3: // 270 deg
                sin = -1;
                cos = 0;
                break;
            }

            return integer_matrix_3x3
            {{{
                {{  cos, sin, 0 }},
                {{ -sin, cos, 0 }},
                {{    0,   0, 1 }},
            }}};
        }

        static integer_matrix_3x3 rotate_ccw(int n = 1)
        {
            return rotate_cw(-n);
        }

        static integer_matrix_3x3 translate(coord by)
        {
            return integer_matrix_3x3
            {{{
                {{    1,    0, 0 }},
                {{    0,    1, 0 }},
                {{ by.x, by.y, 1 }},
            }}};
        }

        static integer_matrix_3x3 hmirror()
        {
            return integer_matrix_3x3
            {{{
                {{ -1, 0, 0 }},
                {{  0, 1, 0 }},
                {{  0, 0, 1 }},
            }}};
        }

        static integer_matrix_3x3 vmirror()
        {
            return integer_matrix_3x3
            {{{
                {{ 1,  0, 0 }},
                {{ 0, -1, 0 }},
                {{ 0,  0, 1 }},
            }}};
        }
    };

    using imat3x3 = integer_matrix_3x3<int>;
    using lmat3x3 = integer_matrix_3x3<long>;
    using llmat3x3 = integer_matrix_3x3<long long>;

    template<typename T>
    bool operator==(integer_matrix_3x3<T> lhs, integer_matrix_3x3<T> rhs)
    {
        return lhs.arr == rhs.arr;
    }

    template<typename T>
    bool operator!=(integer_matrix_3x3<T> lhs, integer_matrix_3x3<T> rhs)
    {
        return lhs.arr != rhs.arr;
    }

    template<typename T>
    bool operator<(integer_matrix_3x3<T> lhs, integer_matrix_3x3<T> rhs)
    {
        return lhs.arr < rhs.arr;
    }

    template<typename T>
    integer_matrix_3x3<T> operator*(integer_matrix_3x3<T> lhs,
                                    integer_matrix_3x3<T> rhs)
    {
        integer_matrix_3x3<T> result;
        for(std::size_t i = 0; i != 3; ++i)
        for(std::size_t j = 0; j != 3; ++j)
        {
            result[i][j] = 0;
            for(int k = 0; k != 3; ++k)
                result[i][j] += lhs[i][k] * rhs[k][j];
        }
        return result;
    }

    template<typename T>
    integer_matrix_3x3<T>& operator*=(integer_matrix_3x3<T>& lhs,
                                      integer_matrix_3x3<T> rhs)
    {
        lhs = lhs * rhs;
        return lhs;
    }

    template<typename T>
    coord transform(integer_matrix_3x3<T> mat, coord crd)
    {
        return
        {
            mat[0][0]*crd.x + mat[1][0]*crd.y + mat[2][0],
            mat[0][1]*crd.x + mat[1][1]*crd.y + mat[2][1],
        };
    }

    template<typename T>
    rect transform(integer_matrix_3x3<T> mat, rect r)
    {
        return rect_from_2_coords(transform(mat, r.c), transform(mat,r.r()));
    }

    template<typename T>
    T determinant(integer_matrix_3x3<T> m)
    {
        return (+ m[0][0]*m[1][1]*m[2][2]
                + m[0][1]*m[1][2]*m[2][0]
                + m[0][2]*m[1][0]*m[2][1]
                - m[0][2]*m[1][1]*m[2][0]
                - m[0][1]*m[1][0]*m[2][2]
                - m[0][0]*m[1][2]*m[2][1]);
    }

}

#endif
